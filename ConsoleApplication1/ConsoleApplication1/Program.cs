﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        
        
            static void f(int x)
        {
            if (x > 0)
            {
                //System.Console.WriteLine(x);
                f(x - 1);
                //System.Console.WriteLine(x);
            }
        }
        static void Main(string[] args)
        {
            f2(5);
            Console.Read();
        }
        
        static void f2(int x)
        {
            int y = x;
            var stack1=new Stack<int>();
            stack1.Push(x);
            bool dir = true;
            while(stack1.Count!=0)
            {
                x = stack1.Peek();
                stack1.Pop();
                Console.WriteLine(x);
                if(x>0 && dir)
                {
                    stack1.Push(x);
                    stack1.Push(x - 1);
                }
                else
                {
                    dir = false;
                }
            }

        }
    }
}

